// TypeScript Types and how they work
// string
var myName = 'Cornell';
// myName = 43; TypeScript is explicit, this will produce an Error. Unlike JavaScript TS does not have Dynamic Types.
// number
var myAge = 43;
myAge = 43.7;
// numbers can be integers or floating type
// boolean
var hasHobbies = true;
// hasHobbies = 1; Returns error
// assign types
var myRealAge;
// let myRealAge: number; This is explicit
myRealAge = 27;
myRealAge = '27';
// This will not produce an error because TS assigns this variable as Type: any?
// array
var hobbies = ['Cooking', 'Music'];
// hobbies = [10, 20, 30]; // This will produce an error due to TS assigning Type: string to the array
// if we set let hobbies: any[] = ['Cooking', 'Music']; We could create a number array and it would work
console.log('Array Val: ', hobbies[0]);
console.log('Array Val2: ', typeof hobbies); // Will console the whole Object
// tuples | Arrays with mixed Types and a limited number of items
var address = ['TheStreet', 3412];
// let address: [string, number] = [3412, "TheStreet"]; This would produce an error because a tupal must be in this order set by the Type
// enums
var Color;
(function (Color) {
    Color[Color["Gray"] = 0] = "Gray";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue"; //2
})(Color || (Color = {}));
var myColor = Color.Blue;
console.log('Enums Val: ', myColor); // This will show the place within the Enum not the text.
// any
// Try to never use this Type
var car = 'Bugatti';
console.log(car);
car = { brand: 'Bugatti', series: 'Chiron' };
console.log(car);
// Functions
function returnMyName() {
    // Will only allow for the return of Type string
    return myName; // return myAge; will produce Type error
}
console.log(returnMyName());
// void functions | Should not return anything
function sayHello() {
    console.log('Hello!');
}
// Argument Types
function multiply(val1, val2) {
    return val1 * val2;
}
console.log('Multiply: ', multiply(3, myName)); // Will compile, but will give a result of NaN
function multiply2(val1, val2) {
    return val1 * val2;
}
// console.log(multiply2(3, myName)); // This will result in an error
console.log('Multiply2: ', multiply2(765, 6534));
// Function Types
var myMultiply; // This is a function Type
// myMultiply = sayHello; // error Type void is unassignable to Type number
// console.log('mMulti 1st: ', myMultiply(10, 2)); // In this case myMultiply is not a function, only a Type, so you recieve a console error
myMultiply = multiply2;
console.log('mMulti 2nd: ', myMultiply(10, 6)); // By assigning a function value to myMultiply this will display a result in the console
// Objects
var userData = {
    name: 'C.Coulon',
    age: 43
};
// userData = { // This will create an error due in TS because the properties within the object must match
//   n: 'C.Coulon',
//   a: 43
// };
// Complex Object
var complex = {
    data: [100, 3.99, 10],
    output: function (all) {
        return this.data;
    }
};
// let complex2: Complex = {
//   data: [100, 3.99, 10],
//   output: function (all: boolean): number[] {
//     return this.data;
//   }
// };
// Union Types
var myRealRealAge = 27; // Can assign multiple types
myRealRealAge = '27';
// myRealRealAge = true; //Type 'true' is not assignable to type 'string | number'.
// Check Types in your code
var finalValue = 'A string';
if (typeof finalValue == 'number') {
    console.log('finalValue is a number.');
}
else {
    console.log('finalValue is a string.');
}
// Never Type
function neverReturns() {
    // should only be executed if your code reaches this point. The concept of never is more of a testing feature
    throw new Error('An error!');
}
// Nullable Types
var canBeNull = 12; // let canBeNull: number | null = 12; If we assign a union Type canBeNull will compile
// canBeNull = null; //Type 'null' is not assignable to type 'number'. tsconfig.json - line 7
var canAlsoBeNull; //undefined
canAlsoBeNull = null;
