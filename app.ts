// TypeScript Types and how they work

// string
let myName = 'Cornell';
// myName = 43; TypeScript is explicit, this will produce an Error. Unlike JavaScript TS does not have Dynamic Types.

// number
let myAge = 43;
myAge = 43.7;
// numbers can be integers or floating type

// boolean
let hasHobbies = true;
// hasHobbies = 1; Returns error

// assign types
let myRealAge;
// let myRealAge: number; This is explicit
myRealAge = 27;
myRealAge = '27';
// This will not produce an error because TS assigns this variable as Type: any?

// array
let hobbies = ['Cooking', 'Music'];
// hobbies = [10, 20, 30]; // This will produce an error due to TS assigning Type: string to the array
// if we set let hobbies: any[] = ['Cooking', 'Music']; We could create a number array and it would work
console.log('Array Val: ', hobbies[0]);
console.log('Array Val2: ', typeof hobbies); // Will console the whole Object

// tuples | Arrays with mixed Types and a limited number of items
let address: [string, number] = ['TheStreet', 3412];
// let address: [string, number] = [3412, "TheStreet"]; This would produce an error because a tupal must be in this order set by the Type

// enums
enum Color {
  Gray, // 0
  Green, //1 if you set the value of Green = 100, the value of Blue = 101, because it will increment from the previous value
  Blue //2
}
let myColor: Color = Color.Blue;
console.log('Enums Val: ', myColor); // This will show the place within the Enum not the text.

// any
// Try to never use this Type
let car: any = 'Bugatti';
console.log(car);
car = { brand: 'Bugatti', series: 'Chiron' };
console.log(car);

// Functions
function returnMyName(): string {
  // Will only allow for the return of Type string
  return myName; // return myAge; will produce Type error
}
console.log(returnMyName());

// void functions | Should not return anything
function sayHello(): void {
  console.log('Hello!');
}

// Argument Types
function multiply(val1, val2): number {
  return val1 * val2;
}
console.log('Multiply: ', multiply(3, myName)); // Will compile, but will give a result of NaN

function multiply2(val1: number, val2: number): number {
  return val1 * val2;
}
// console.log(multiply2(3, myName)); // This will result in an error
console.log('Multiply2: ', multiply2(765, 6534));

// Function Types
let myMultiply: (val1: number, val2: number) => number; // This is a function Type
// myMultiply = sayHello; // error Type void is unassignable to Type number
// console.log('mMulti 1st: ', myMultiply(10, 2)); // In this case myMultiply is not a function, only a Type, so you recieve a console error
myMultiply = multiply2;
console.log('mMulti 2nd: ', myMultiply(10, 6)); // By assigning a function value to myMultiply this will display a result in the console

// Objects
let userData: { name: string; age: number } = {
  name: 'C.Coulon',
  age: 43
};

// userData = { // This will create an error due in TS because the properties within the object must match
//   n: 'C.Coulon',
//   a: 43
// };

// Complex Object
let complex: { data: number[]; output: (all: boolean) => number[] } = {
  data: [100, 3.99, 10],

  output: function(all: boolean): number[] {
    return this.data;
  }
};
// complex = {}; // This creates an error because we cannot assign a simple Object Type to a complex Type

// Type Alias
type Complex = { data: number[]; output: (all: boolean) => number[] };

// let complex2: Complex = {
//   data: [100, 3.99, 10],

//   output: function (all: boolean): number[] {
//     return this.data;
//   }
// };

// Union Types
let myRealRealAge: number | string = 27; // Can assign multiple types
myRealRealAge = '27';
// myRealRealAge = true; //Type 'true' is not assignable to type 'string | number'.

// Check Types in your code
let finalValue = 'A string';
if (typeof finalValue == 'number') {
  console.log('finalValue is a number.');
} else {
  console.log('finalValue is a string.');
}

// Never Type
function neverReturns(): never {
  // should only be executed if your code reaches this point. The concept of never is more of a testing feature
  throw new Error('An error!');
}

// Nullable Types
let canBeNull = 12; // let canBeNull: number | null = 12; If we assign a union Type canBeNull will compile
// canBeNull = null; //Type 'null' is not assignable to type 'number'. tsconfig.json - line 7
let canAlsoBeNull; //undefined
canAlsoBeNull = null;

// Example
type BankAccount = { money: number; deposit: (val: number) => void };

let bankAccount: BankAccount = {
  money: 2000,
  deposit(value: number) {
    this.money += value;
  }
};

let myself: { name: string; bankAccount: BankAccount; hobbies: string[] } = {
  name: 'Max',
  bankAccount: bankAccount,
  hobbies: ['Sports', 'Cooking']
};

myself.bankAccount.deposit(3000);

console.log(myself);

function controlMe(isTrue: boolean) {
  let result: number;
  if (isTrue) {
    result = 12;
    return result;
  } else {
    result = 33;
    return result;
  }
}
